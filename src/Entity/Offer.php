<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FlightDetailsRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FlightDetailsRepository::class)
 * @UniqueEntity(
 *     fields={"flight", "armchair"},
 *     errorPath="armchair",
 *     message="This armchair is already in use on that flight."
 * )
 */
class Offer
{

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updateAt = new DateTime();
        $this->status = 1;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 4,
     *      notInRangeMessage = "Enter the status from 1 or 2,3,4 where 1-reservation, 2-purchase, 3-return, 4-cancel"
     * )
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @ORM\OneToOne(targetEntity=Passenger::class, inversedBy="flight")
     */
    private $passenger;

    /**
     * @ORM\ManyToOne(targetEntity=Flight::class, inversedBy="passengers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $flight;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 150,
     *      notInRangeMessage = "Select a chair from {{ min }} to {{ max }} to continue"
     * )
     */
    private $armchair;

    public function __toString()
    {
       return "Offer";
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getPassenger(): ?Passenger
    {
        return $this->passenger;
    }

    public function setPassenger(?Passenger $passenger): self
    {
        $this->passenger = $passenger;

        return $this;
    }

    public function getFlight(): ?Flight
    {
        return $this->flight;
    }

    public function setFlight(?Flight $flight): self
    {
        $this->flight = $flight;

        return $this;
    }

    public function getArmchair(): ?int
    {
        return $this->armchair;
    }

    public function setArmchair(int $armchair): self
    {
        $this->armchair = $armchair;

        return $this;
    }

}
