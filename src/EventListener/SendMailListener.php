<?php
//src/EventListener/ExceptionListener.php
namespace App\EventListener;

use App\Entity\Flight;
use App\Entity\Offer;
use App\Entity\Passenger;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SendMailListener
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    private $offers;
    private $flight;
    private $transport;

    /**
     * SendMailListener constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param $offers
     * @param $flight Flight
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, $offers, $flight)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->offers          = $offers;
        $this->flight          = $flight;
        $this->transport        = $this->getTransport();
    }

    /**
     * @param Event $event
     * @return Event
     */
    public function onTerminate(Event $event)
    {
        /**@var $offer Offer **/
        foreach ($this->offers as $offer) {
            $offer->getPassenger()->getEmail();
            $this->sendEmail($offer->getPassenger(), $this->flight->getTitle() );
        }
        return $event;
    }

    /**
     * @param Passenger $passenger
     * @param $title
     */
    public function sendEmail($passenger, $title)
    {
        $mailer    = new Swift_Mailer($this->transport);
        $message   = (new Swift_Message("Ваш рейс, {$title} был отменен"))
            ->setFrom('s7airlinessss@gmail.com')
            ->setTo($passenger->getEmail())
            ->setBody("Здравствуйте {$passenger->getLastName()} {$passenger->getFirstName()}, нам очень жаль, Ваш рейст был отмент, для получение подробностей обратитесь в службу поддержки")
        ;
        $mailer->send($message);
    }

    /**
     * @return \Swift_SmtpTransport
     */
    public function getTransport(){
        $transport = new \Swift_SmtpTransport('smtp.gmail.com',587);
        $transport->setUsername('s7airlinessss@gmail.com');
        $transport->setPassword('Password123!@#');
        $transport->setEncryption('TLS');
        return $transport;
    }
}