<?php

namespace App\Controller;

use App\Entity\Flight;
use App\Entity\Offer;
use App\Model\EventModel;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EventController extends AbstractController
{
    /**
     * @var EventModel
     */
    private $model;
    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * EventController constructor.
     */
    public function __construct()
    {
        $this->eventDispatcher = new EventDispatcher();
        $this->model = new EventModel($this->eventDispatcher);
    }

    /**
     * @return Response
     */
    public function callback()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);

        if (!$this->model->isValid($data)){
            return new Response('Bad request', 401);
        }

        $data       = $data['data'];
        $flight_id  = $data['flight_id'];
        $repo       = $this->getDoctrine()->getRepository(Flight::class);
        /**@var $flight Flight**/
        $flight     = $repo->find($flight_id);
        $method     = $data['event'];
        $result     = false;
        if (method_exists($this, $method)) {
            $result = $this->$method($flight);
        } else {
            $result = $this->flight_ticket_cancelled($flight);
        }

        return $this->json([
            'status' => $result ? 'ok' : 'error(event not exist)',
            'message' => 'ok'],200
        );
    }

    /**
     * @param Flight $flight
     * @return bool
     */
    private function flight_ticket_sales_completed(Flight $flight){
        $flight->setIsCompleted(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($flight);
        $em->flush();
        return true;
    }

    /**
     * @param Flight $flight
     * @return bool
     */
    private function flight_ticket_cancelled(Flight $flight){
        $offers     = $flight->getOffers();
        /*Подпись на событие*/
        $this->model->subscribeEvent($offers,$flight);
        /*Вызов собитие TERMINATE*/
        $this->eventDispatcher->dispatch(KernelEvents::TERMINATE);
        return true;
    }

}
