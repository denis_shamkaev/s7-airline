<?php
namespace App\Model;
use App\EventListener\SendMailListener;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\KernelEvents;

class EventModel{
    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * EventModel constructor.
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param $offers
     * @param $flight
     */
    public function subscribeEvent($offers,$flight){
        $listener = new SendMailListener($this->eventDispatcher,$offers,$flight);
        $this->eventDispatcher->addListener(KernelEvents::TERMINATE,[$listener, 'onTerminate']);
    }

    /**
     * @param $data
     * @return bool
     */
    public function isValid($data){
        if (!isset($data['data'])){
            return false;
        }

        $data   = $data['data'];
        $fields = ['flight_id','triggered_at','event','secret_key'];

        foreach ($fields as $field) {
            if (!isset($data[$field])){
                return false;
            }
        }
        return true;
    }
}